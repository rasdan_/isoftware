package Estudiante;

public class Estudiante {

	public String nombre;
	public Integer codigo;
	public Integer nota;
	
	public Estudiante(String nombre, Integer codigo, Integer nota){
		this.nombre = nombre;
		this.codigo = codigo;
		this.nota = nota;
	}
}
